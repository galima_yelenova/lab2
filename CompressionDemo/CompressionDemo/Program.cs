﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
namespace CompressionDemo
{
    class Program
    { /*
        static void CompressFile(string inFileName, string outFileName) 
        {
            FileStream sourcefile = File.OpenRead(inFileName);
            FileStream destFile = File.Create(outFileName);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourcefile.ReadByte();
            while (theByte > -1)
            {
                compStream.WriteByte((byte) theByte);
                theByte = sourcefile.ReadByte();
            }
        }
        */
        static void UncompressFile(string inFileName, string outFileName)
        {
            FileStream sourcefile = File.OpenRead(inFileName);
            FileStream destFile = File.Create(outFileName);
            GZipStream compStream = new GZipStream(sourcefile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte > -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }


        }
        static void Main(string[] args)
        {
            //CompressFile(@"c:\soft\newfile.txt", @"c:\soft\newfile.txt.gz");
            UncompressFile(@"c:\soft\newfile.txt.gz", @"c:\soft\newfile.txt.test");
        }
    }
}
